<?php

namespace App;

/**
 * The main Tester.
 */
class Tester
{
    public function runTest($testName)
    {
        $test = null;

        switch ($testName) {
            case 'adapter':
                $test = new Adapter\AdapterTest();
                break;

            case 'factory':
                $test = new Factory\FactoryTest();
                break;

            case 'strategy':
                $test = new Strategy\StrategyTest();
                break;

            case 'builder':
                $test = new Builder\BuilderTest();
                break;

            case '':
                echo 'No pattern selected.';
                break;

            default:
                echo 'Invalid pattern selected.';
                break;
        }

        if (!empty($test)) {
            echo '<h2>'.$test->getName().'</h2>';
            $test->run();
        }
    }
}
