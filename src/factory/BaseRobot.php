<?php

namespace App\factory;

/**
 * The abstract robot class.
 */
abstract class BaseRobot
{
    private $name;
    private $speed;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSpeed()
    {
        return $this->speen;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    public function doRobotDance()
    {
        if ($this->speed > 7) {
            echo "We're doing the quick robot dance!<br>";
        } else {
            echo "We're doing the slow robot dance!<br>";
        }

        return true;
    }
}
