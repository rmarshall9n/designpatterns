<?php

namespace App\factory;

class BigRobot extends BaseRobot
{
    public function __construct()
    {
        $this->setName('Big dancing robot.');
        $this->setSpeed(5);
    }
}
