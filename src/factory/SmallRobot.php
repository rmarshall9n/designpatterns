<?php

namespace App\factory;

class SmallRobot extends BaseRobot
{
    public function __construct()
    {
        $this->setName('Small dancing robot.');
        $this->setSpeed(10);
    }
}
