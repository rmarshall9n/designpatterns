<?php

namespace App\factory;

/**
 * Robot Factory - design pattern.
 */
class RobotFactory
{
    const BIG_ROBOT = 1;
    const SMALL_ROBOT = 2;

    public function create($robotType)
    {
        $robot = null;

        switch ($robotType) {
            case self::BIG_ROBOT:
                $robot = new BigRobot();
                break;

            case self::SMALL_ROBOT:
                $robot = new SmallRobot();
                break;

            default:
                break;
        }

        return $robot;
    }
}
