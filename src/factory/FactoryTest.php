<?php

namespace App\factory;

/**
 * Test case for Robot Factory.
 */
class FactoryTest extends \App\TestBase
{
    public function __construct()
    {
        $this->setName('Factory');
    }

    public function run()
    {
        $robotFactory = new RobotFactory();

        $bigRobot = $robotFactory->create(RobotFactory::BIG_ROBOT);
        echo $bigRobot->getName().'<br>';
        $bigRobot->doRobotDance();

        echo '<br>';
        $smallRobot = $robotFactory->create(RobotFactory::SMALL_ROBOT);
        echo $smallRobot->getName().'<br>';
        $smallRobot->doRobotDance();
    }
}
