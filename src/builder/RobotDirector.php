<?php

namespace App\builder;

/**
 * Robot director for building robots.
 */
class RobotDirector
{
    private $robotBuilder;

    public function __construct($robotBuilder)
    {
        $this->robotBuilder = $robotBuilder;
    }

    public function getRobot()
    {
        return $this->robotBuilder->getRobot();
    }

    public function makeRobot()
    {
        $this->robotBuilder->buildRobotHead();
        $this->robotBuilder->buildRobotBody();
        $this->robotBuilder->buildRobotArms();
        $this->robotBuilder->buildRobotLegs();
    }
}
