<?php

namespace App\builder;

/**
 * Robot.
 */
class Robot implements IRobotPlan
{
    private $head;
    private $body;
    private $arms;
    private $legs;

    public function __construct()
    {
    }

    // setters
    public function setRobotHead($part)
    {
        $this->head = $part;
    }

    public function setRobotBody($part)
    {
        $this->body = $part;
    }

    public function setRobotArms($part)
    {
        $this->arms = $part;
    }

    public function setRobotLegs($part)
    {
        $this->legs = $part;
    }

    // getters
    public function getRobotHead()
    {
        return $this->head;
    }

    public function getRobotBody()
    {
        return $this->body;
    }

    public function getRobotArms()
    {
        return $this->arms;
    }

    public function getRobotLegs()
    {
        return $this->legs;
    }
}
