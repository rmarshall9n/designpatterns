<?php

namespace App\builder;

/**
 * Robot Plan Interface.
 */
interface iRobotPlan
{
    public function setRobotHead($part);
    public function setRobotBody($part);
    public function setRobotArms($part);
    public function setRobotLegs($part);
}
