<?php

namespace App\builder;

/**
 * Robot builder interface.
 */
interface iRobotBuilder
{
    public function buildRobotHead();
    public function buildRobotBody();
    public function buildRobotArms();
    public function buildRobotLegs();
    public function getRobot();
}
