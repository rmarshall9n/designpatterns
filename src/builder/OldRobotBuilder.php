<?php

namespace App\builder;

/**
 * builder for old robot.
 */
class OldRobotBuilder implements iRobotBuilder
{
    private $robot;

    public function __construct()
    {
        $this->robot = new Robot();
    }

    public function buildRobotHead()
    {
        $this->robot->setRobotHead('Old Head.');
    }

    public function buildRobotBody()
    {
        $this->robot->setRobotBody('Old Body.');
    }

    public function buildRobotArms()
    {
        $this->robot->setRobotArms('Old Arms.');
    }

    public function buildRobotLegs()
    {
        $this->robot->setRobotLegs('Old Legs.');
    }

    public function getRobot()
    {
        return $this->robot;
    }
}
