<?php

namespace App\builder;

/**
 * Test case for Robot Builder.
 */
class BuilderTest extends \App\TestBase
{
    public function __construct()
    {
        $this->setName('Builder');
    }

    public function run()
    {
        $oldRobot = new OldRobotBuilder();

        $director = new RobotDirector($oldRobot);

        $director->makeRobot();

        $firsRobot = $director->getRobot();

        echo 'Head: '.$firsRobot->getRobotHead().'<br />';
        echo 'Body: '.$firsRobot->getRobotBody().'<br />';
        echo 'Arms: '.$firsRobot->getRobotArms().'<br />';
        echo 'Legs: '.$firsRobot->getRobotLegs().'<br />';
    }
}
