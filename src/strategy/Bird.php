<?php

namespace App\strategy;

/**
 * Bird animal class.
 */
class Bird extends AnimalBase
{
    public function __construct()
    {
        $this->flyingType = new FlyWings();
    }
}
