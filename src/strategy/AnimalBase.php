<?php

namespace App\strategy;

/**
 * Base class for an animal.
 */
class AnimalBase
{
    public $flyingType;

    public function __construct()
    {
    }

    public function getFlyingType()
    {
        return $this->flyingType;
    }

    public function setFlyingType($flyingType)
    {
        $this->flyingType = $flyingType;
    }

    public function fly()
    {
        $this->flyingType->fly();
    }
}
