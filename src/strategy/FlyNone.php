<?php

namespace App\strategy;

class FlyNone implements FlyInterface
{
    public function fly()
    {
        echo "I can't fly.";
    }
}
