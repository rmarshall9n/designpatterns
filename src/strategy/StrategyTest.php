<?php

namespace App\strategy;

/**
 * Test case for Robot Strategy.
 */
class StrategyTest extends \App\TestBase
{
    public function __construct()
    {
        $this->setName('Strategy');
    }

    public function run()
    {
        $cat = new Cat();

        echo 'Test the cat:<br>';
        $cat->fly();

        echo '<br>Equip the wings:';
        $cat->setFlyingType((new FlyWings()));
        echo '<br/ >';
        $cat->fly();

        echo '<br/ >';
        echo '<br/ >';
        echo 'Test the bird:<br>';
        $bird = new Bird();
        $bird->fly();
    }
}
