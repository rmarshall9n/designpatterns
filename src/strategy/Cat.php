<?php

namespace App\strategy;

/**
 * Cat animal class.
 */
class Cat extends AnimalBase
{
    public function __construct()
    {
        $this->flyingType = new FlyNone();
    }
}
