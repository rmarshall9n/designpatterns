<?php

namespace App\strategy;

class FlyWings implements FlyInterface
{
    public function fly()
    {
        echo 'Flying with wings.';
    }
}
