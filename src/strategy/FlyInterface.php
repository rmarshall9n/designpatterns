<?php

namespace App\strategy;

/**
 * The interface to fly with.
 */
interface FlyInterface
{
    public function fly();
}
