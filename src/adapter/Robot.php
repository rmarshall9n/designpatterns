<?php

namespace App\adapter;

class Robot
{
    public function __construct()
    {
    }

    public function stomp()
    {
        echo 'Stomping';
    }

    public function rollForward()
    {
        echo 'Rolling Forward';
    }
}
