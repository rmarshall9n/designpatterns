<?php

namespace App\adapter;

/**
 * Test case for Robot Adaper.
 */
class AdapterTest extends \App\TestBase
{
    public function __construct()
    {
        $this->setName('AdapterTest');
    }

    public function run()
    {
        echo 'Testing the Monster:<br>';
        $monster = new Monster();
        $monster->moveForward();
        echo '<br />';
        $monster->attack();
        echo '<br />';
        $monster->equipWeapon('sword');
        $monster->attack();
        echo '<br />';

        echo '<br><br>';
        echo 'Testing the Robot Adapter:<br>';
        $robot = new RobotAdapter();
        $robot->moveForward();
        echo '<br />';
        $robot->attack();
        echo '<br />';
        $robot->equipWeapon('sword');
        echo '<br />';
        $robot->attack();
        echo '<br />';
    }
}
