<?php

namespace App\adapter;

class Monster implements EnemyInterface
{
    public $weapon = 'Hammer';

    public function attack()
    {
        echo 'Monster attacking with a '.$this->weapon;
    }

    public function moveForward()
    {
        echo 'Monster Moving Forward!';
    }

    public function equipWeapon($weapon)
    {
        $this->weapon = $weapon;
    }
}
