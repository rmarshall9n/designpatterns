<?php

namespace App\adapter;

interface EnemyInterface
{
    public function attack();
    public function moveForward();
    public function equipWeapon($weapon);
}
