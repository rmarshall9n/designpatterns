<?php

namespace App\adapter;

class RobotAdapter implements EnemyInterface
{
    private $_robot;

    public function __construct()
    {
        $this->_robot = new Robot();
    }

    public function attack()
    {
        $this->_robot->stomp();
    }

    public function moveForward()
    {
        $this->_robot->rollForward();
    }

    public function equipWeapon($weapon)
    {
        echo 'Robots cant hold weapons.';
    }
}
