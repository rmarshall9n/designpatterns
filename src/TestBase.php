<?php

namespace App;

/**
 * Base test class.
 */
abstract class TestBase
{
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    protected function setName($name)
    {
        $this->name = $name;
    }

    public function run()
    {
    }
}
