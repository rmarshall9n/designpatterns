<?php

require_once __DIR__.'/vendor/autoload.php';

$tester = new App\Tester();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Design Patterns</title>
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">

    <style type="text/css">
        html, body, .fill {
            min-height: 100%;
            height: 100%;
        }
    </style>

</head>
<body style="background-color: #cccccc;">

    <div class="container fill" style="background-color: white;">
        <div class="row">
            <div class="col-md-12">

                <h1>Design Patterns</h1>
                <ul class="nav nav-pills">
                    <li role="presentation"><a href="?pattern=adapter">Adapter</a></li>
                    <li role="presentation"><a href="?pattern=factory">Factory</a></li>
                    <li role="presentation"><a href="?pattern=strategy">Strategy</a></li>
                    <li role="presentation"><a href="?pattern=builder">Builder</a></li>
                </ul>

                <?php
                if (isset($_GET['pattern'])) {
                    $tester->runTest($_GET['pattern']);
                }
                ?>
            </div>
        </div>
    </div>

</body>
</html>